import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.familyForm = new FormGroup({
      name: new FormControl('', Validators.required),
      father: this.createFamilyMember(),
      mother: this.createFamilyMember(),
      children: new FormArray([this.createFamilyMember()]),
    })
  }

  public createFamilyMember(): FormGroup {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      age: new FormControl('', Validators.required),
    })
  }

  public addChild(): void{
    this.children.push(this.createFamilyMember());
  }

  public removeChild(index: number): void{
    this.children.removeAt(index);
  }

  public submit(): void{
    this.familyService.addFamily$(this.familyForm.value).subscribe();
  }
}
